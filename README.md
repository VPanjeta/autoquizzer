# AutoQuizzer

#Steps to build the project

1. cd into the backend repository and run `pip3 install -r requirements.txt`  
2. Edit the config.py to add authorization data.
3. Run `python3 astra_handler.py --make-migrations` to create the table on Astra DB.
4. Run `python3 application.py` to the backend server and keep the server running.
5. Open Chrome and go to add extensions and add a test extensions.
6. Browse to the browser-plugin repositoy and select the `manifest.json` file to add browser to chrome.
7. Open youtube and click on the extension to start the quiz.

