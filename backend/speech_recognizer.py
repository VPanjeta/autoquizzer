# #!/usr/bin/env python
# # coding: utf-8

# # Copyright (c) Microsoft. All rights reserved.
# # Licensed under the MIT license. See LICENSE.md file in the project root for full license information.
# """
# Speech recognition samples for the Microsoft Cognitive Services Speech SDK
# """

# import time
# import wave
# import os
# from os import path
# import utilities

# # try:
# import azure.cognitiveservices.speech as speechsdk
# # except ImportError:
# #     print("""
# #     Importing the Speech SDK for Python failed.
# #     Refer to
# #     https://docs.microsoft.com/azure/cognitive-services/speech-service/quickstart-python for
# #     installation instructions.
# #     """)
# #     import sys
# #     sys.exit(1)


# # Set up the subscription info for the Speech Service:
# # Replace with your own subscription key and service region (e.g., "westus").
# speech_key, service_region = "", ""

# # Specify the path to an audio file containing speech (mono WAV / PCM with a sampling rate of 16
# # kHz).
# #weatherfilename = "/Users/smantm/Downloads/tonsils.wav"
# audio_filepath = 'Audio_Files/'
# text_filepath = 'Text_Files/'
# def get_sentences(audio_id):

#     """performs continuous speech recognition with input from an audio file"""
#     print('Audio ID in speech rec')
#     print(audio_id)
#     text_file_path = text_filepath+audio_id+'.txt'
#     if  os.path.exists(text_file_path):
#         return 
#     # <SpeechContinuousRecognitionWithFile>
#     speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region)
#     speech_config.request_word_level_timestamps()

#     audio_config = speechsdk.audio.AudioConfig(filename=audio_filepath+audio_id+'.wav')

#     speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config, audio_config=audio_config)



#     done = False

#     def stop_cb(evt):
#         """callback that signals to stop continuous recognition upon receiving an event `evt`"""
#         f.close()
#         print('CLOSING on {}'.format(evt))
#         nonlocal done
#         done = True
    
#     #have to change
#     f = open(text_filepath+audio_id+".txt" , "a")
#     def abc(evt):
#         #print(evt)
#         print(evt.result.json)
#         f.write('{}'.format(evt.result.text))
        
#     # Connect callbacks to the events fired by the speech recognizer
#     # speech_recognizer.recognizing.connect(lambda evt: print(''))
#     speech_recognizer.recognized.connect(abc)
#     speech_recognizer.session_started.connect(lambda evt: print('SESSION STARTED: {}'.format(evt)))
#     speech_recognizer.session_stopped.connect(lambda evt: print('SESSION STOPPED {}'.format(evt)))
#     speech_recognizer.canceled.connect(lambda evt: print('CANCELED {}'.format(evt)))
#     # stop continuous recognition on either session stopped or canceled events
#     speech_recognizer.session_stopped.connect(stop_cb)
#     speech_recognizer.canceled.connect(stop_cb)

#     # Start continuous speech recognition
#     speech_recognizer.start_continuous_recognition()
#     while not done:
#         time.sleep(.5)
#     speech_recognizer.stop_continuous_recognition()
    
#     # </SpeechContinuousRecognitionWithFile>

# # get_sentences('UPBMG5EYydo')