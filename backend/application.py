import flask
import question_generator 
import summarizer 
import speech_recognizer
import utilities 
import os
from os import path
import re
from flask import request
import vtt_to_txt
from flask_cors import CORS
import json

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

text_folder_path = 'Text_Files/'
vtt_folder_path = 'VTT_Files/'
json_folder_path = 'JSON_Files/'

@app.route('/', methods=['GET'])
def home():
    v = request.args.get('v')
    if not v:
        return {"Issue":"Please pass video ID v in query params"}

    url = 'https://www.youtube.com/watch?v=' + v
    #get audio 
    # video_id = utilities.get_audio_file(url)
    video_id = utilities.get_vtt_file(url)
    #process audio and get text
    #has to improve
    text_file_path = text_folder_path+video_id+'.txt'
    json_file_path = json_folder_path+video_id+'.json'

    # use VTT to get text
    # json_file_path = json_folder_path+video_id+'.json'
    vtt_to_txt.convert_to_text(video_id)

    # use Azure speech_recognizer to get text from audio
    # speech_recognizer.get_sentences(video_id)

    # get summary
    summary = summarizer.get_summary(open(text_file_path,'r').read())
    
    

    # Add space after full stop
    rx = r"\.(?=\S)"
    summary = re.sub(rx, ". ", summary)
    re.sub(".", ". ",summary)

    # get questions and return
    json_sentences = json.loads(open(json_file_path,'r').read())

    questions = question_generator.generate_trivia(summary)
    prevJumpToTime = 0
    for question in questions:
        correct_index = question['correctIndex']
        correct_answer = question['answers'][correct_index] 
        for json_sentence in json_sentences:
            json_sentence_time = utilities.get_sec(json_sentence['time'])
            if json_sentence_time >= prevJumpToTime and json_sentence['line'].find(correct_answer) >= 0:
                question['jumpToTime'] = json_sentence_time
                prevJumpToTime = json_sentence_time
                break

    print(questions)
    return json.dumps(questions)

app.run()


print(home())