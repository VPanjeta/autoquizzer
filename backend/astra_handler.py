import requests
import json
from config import *
import sys


def save_data(id, url, answer_data):

    headers = {
        'Content-Type': 'application/json',
        'x-cassandra-token': TOKEN
    }

    payload = {
        "columns": [
            {
                "name": "id",
                "value": id
            },
            {
                "name": "uri",
                "value": url
            },
            {
                "name": "answer_data",
                "value": json.dumps(answer_data)
            }
        ]
    }

    res = requests.post(BASE_URL + f"/tables/{TABLE_NAME}/rows", headers=headers, data=json.dumps(payload))


def get_data(id):

    headers = {
        'x-cassandra-token': TOKEN
    }

    res = requests.get(BASE_URL + f"/tables/{TABLE_NAME}/rows/{id}", headers=headers)
    if res.status_code == 201 or res.status_code == 200:
        return res.json()
    else:
        return None



def create_table():

    headers = {
        'Content-Type': 'application/json',
        'x-cassandra-token': TOKEN
    }

    payload = {
        "name": TABLE_NAME,
        "ifNotExists": True,
        "columnDefinitions": [ 
            {
                "name": "id",
                "typeDefinition": "text",
                "static": False
            }, 
            {
                "name": "uri",
                "typeDefinition": "text",
                "static": False
            }, 
            {
                "name": "answer_data",
                "typeDefinition": "text",
                "static": False
            }
        ],
        "primaryKey": {
            "partitionKey": ["id"]
        },
        "tableOptions": {
            "defaultTimeToLive":0
        }
    }

    res = requests.post(BASE_URL + f"/tables", data=json.dumps(payload), headers=headers)

    if res.status_code == 201 or res.status_code == 200:
        print('Table created successfully!!!')
    else:
        print(res.text)



if __name__ == '__main__':
    
    if '--make-migrations' in sys.argv:
        create_table()

    else:
        print('Run python3 astra_handler.py --make-migrations to create table in your workspace.')

