from __future__ import unicode_literals
import youtube_dl
from urllib import parse 
import os
from os import path

audio_file_dir = 'Audio_Files/'
vtt_file_dir = 'VTT_Files/'
#text_file_dir = 'Text_Files/'
def get_audio_file(url):
    video_id = ''.join (c for c in parse.parse_qs(parse.urlsplit(url).query)['v'])
    audio_file_path = audio_file_dir+video_id

    if  os.path.exists(audio_file_path + '.wav'):
        return video_id
    ydl_opts = {
        'format': 'bestaudio/best',
        'extractaudio':True,
        'audioformat':'wav',
        'noplaylist':True,
        'nocheckcertificate':True,
        'outtmpl': audio_file_path+'.%(ext)s',
        'postprocessors' : [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'wav',
            'preferredquality': '192'
            
        }],
        'prefer_ffmpeg': True,
        'keepaudio': True
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])

    return video_id

def get_vtt_file(url):
    video_id = ''.join (c for c in parse.parse_qs(parse.urlsplit(url).query)['v'])
    vtt_file_path = vtt_file_dir+video_id

    if  os.path.exists(vtt_file_path + '.en.vtt'):
        print("\n\n\n here\n\n\n")
        return video_id
    # youtube-dl --skip-download --sub-format vtt --write-auto-sub https://www.youtube.com/watch?v=5DGwOJXSx
    ydl_opts = {
    'skip_download': True,
    'subtitlesformat': 'vtt',
    'writeautomaticsub': True,
    'outtmpl': vtt_file_path+'.%(ext)s',
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])
        print("downloaded")

    return video_id

def get_sec(time_str):
    """Get Seconds from time."""
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)
    
# print(get_vtt_file('https://www.youtube.com/watch?v=_VhcZTGv0CU'))
# print(get_audio_file('https://www.youtube.com/watch?v=UPBMG5EYydo'))