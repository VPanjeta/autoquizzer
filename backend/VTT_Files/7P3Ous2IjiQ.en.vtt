WEBVTT
Kind: captions
Language: en

00:00:05.490 --> 00:00:08.030
Time is something that everyone is familiar with

00:00:08.640 --> 00:00:12.120
60 seconds is one minute, 60 minutes is one hour,

00:00:12.160 --> 00:00:14.280
24 hours is one day, and so on.

00:00:15.160 --> 00:00:19.540
This is known as Linear Time and is something that everyone is familiar with and agrees upon.

00:00:20.500 --> 00:00:27.120
But consider this, if someone came up to you on the street and asked you to draw time, what would you draw?

00:00:27.720 --> 00:00:30.920
You might draw a clock, or a watch ticking every second

00:00:31.320 --> 00:00:35.140
Or you might draw a calendar with X's over each day to represent the passing of time.

00:00:36.080 --> 00:00:41.540
But that's all those drawings would be, just physical representations of the passing of time.

00:00:42.120 --> 00:00:45.829
Those drawings would just scrape the surface of the Enigma that is time.

00:00:46.520 --> 00:00:52.280
Something that seemingly runs our lives and is unavoidable can't be explained by even the smartest people on Earth.

00:00:53.000 --> 00:00:57.160
So what is time and can we prove that time even exists?

00:01:02.780 --> 00:01:07.000
Aristotle once said, "Time is the most unknown of all unknown things."

00:01:07.720 --> 00:01:11.580
That was nearly 2,500 years ago, and it still stands true today.

00:01:12.640 --> 00:01:18.340
If you were to go to Google and type in, "What is time?", you would find that it says time is a dimension,

00:01:18.619 --> 00:01:20.619
and in many ways it is.

00:01:20.690 --> 00:01:25.899
When you text a friend and ask them to meet for coffee, you wouldn't give them a place without a time.

00:01:26.780 --> 00:01:30.280
However, there's a flaw in that definition of time;

00:01:30.420 --> 00:01:34.380
It leaves too many doors unopened - because time is also a measurement.

00:01:35.210 --> 00:01:39.640
For example, I was born in the 1990s. That was over 20 years ago

00:01:40.000 --> 00:01:44.060
If I were to say I was born 18 billion kilometers in the past,

00:01:44.060 --> 00:01:47.860
that wouldn't make much sense and people would probably look at me like I'm crazy.

00:01:48.380 --> 00:01:56.760
With spatial dimensions - the 3D world that we live in - it's very easy to go back and forth between places because these things are essentially fixed in space.

00:01:57.460 --> 00:02:00.360
If I went to the store to buy groceries and I forgot milk,

00:02:00.760 --> 00:02:02.740
I could easily go back and buy some milk.

00:02:03.380 --> 00:02:06.880
However, the time that it took to do that is unable to be retrieved.

00:02:07.700 --> 00:02:10.460
It is lost forever into the past.

00:02:11.560 --> 00:02:15.320
An object placed in 3D space will stay there almost indefinitely.

00:02:15.720 --> 00:02:21.240
If I place a bottle on the table, it will just stay there, but that bottle still falls victim to time.

00:02:21.890 --> 00:02:25.000
See, time is like an arrow - it moves in one direction;

00:02:25.760 --> 00:02:26.560
forward.

00:02:27.260 --> 00:02:29.960
Scientists fittingly called this the arrow of time.

00:02:30.360 --> 00:02:34.460
If you one day woke up and found yourself floating in the middle of empty space,

00:02:34.780 --> 00:02:38.340
would you be able to tell which way is up, down, left or right?

00:02:39.380 --> 00:02:40.740
Probably not.

00:02:41.120 --> 00:02:43.540
However, time is a much simpler ordeal.

00:02:44.060 --> 00:02:49.539
See, time comes from the past, originating at the Big Bang, where our history lies and is fixed.

00:02:49.760 --> 00:02:55.629
Through the present, where we are essentially prisoners of, towards the unknown and turbulent future.

00:02:55.960 --> 00:02:59.700
We can remember things from the past like how I can tell you that this morning

00:02:59.700 --> 00:03:03.180
I went to the store, bought groceries and then forgot to buy milk.

00:03:03.340 --> 00:03:07.180
But at the same time, I can't tell you what I ate for breakfast next Thursday.

00:03:08.530 --> 00:03:13.020
The arrow of time originated at the Big Bang and has been moving forward ever since.

00:03:13.540 --> 00:03:18.330
We used the second law of thermodynamics to represent this. It is known as entropy.

00:03:19.150 --> 00:03:22.710
Think of entropy as a measure of disorder in the universe.

00:03:22.840 --> 00:03:27.140
At The Big Bang, all the matter in the universe was compacted into an infinitely small point.

00:03:27.880 --> 00:03:33.160
This is considered a very low entropy situation; a very orderly situation.

00:03:33.860 --> 00:03:37.460
It would be similar to stuffing every sock that was ever made into one drawer.

00:03:38.140 --> 00:03:42.060
In that situation, you know with 100% certainty where your socks would be.

00:03:42.730 --> 00:03:47.399
Ever since the Big Bang, all the matter in the universe has been expanding away from each other

00:03:47.940 --> 00:03:50.420
making the universe a higher entropy system.

00:03:51.000 --> 00:03:58.080
Because of entropy and because of the arrow of time, we have galaxies, stars, planets, and even life.

00:03:59.500 --> 00:04:02.980
Entropy is the reason that you can tell the difference between the past and the future.

00:04:03.280 --> 00:04:09.400
It explains why every human is born and then lives and then dies - always in that order.

00:04:09.850 --> 00:04:13.079
If there were no entropy -- if there were no change in the universe,

00:04:13.540 --> 00:04:18.080
you wouldn't be able to tell the difference between the year 2017 and the year 1 billion.

00:04:18.580 --> 00:04:24.540
No matter what you do, time moves forward and doesn't stop for anyone or anything.

00:04:25.860 --> 00:04:27.860
At least on the macro scale.

00:04:28.330 --> 00:04:34.799
See, the arrow of time works and is extremely noticeable on large scales the skills that you and I operate on every day.

00:04:35.229 --> 00:04:38.399
But at a quantum level time operates differently.

00:04:39.280 --> 00:04:41.980
Take the situation where you woke up in the middle of space.

00:04:42.540 --> 00:04:46.080
There, you have no idea which way is up, down, left or right.

00:04:46.560 --> 00:04:50.720
It's a very unique situation that only applies in the vastness of empty space,

00:04:51.340 --> 00:04:54.899
but if you come back to Earth, it's very easy for you to orient yourself.

00:04:55.600 --> 00:05:00.359
The arrow of time works in a similar way. On a macro level -- the big level,

00:05:00.460 --> 00:05:04.950
it's very easy for you to tell that the year 1900 is different from the year 2018.

00:05:05.410 --> 00:05:07.559
It's very easy to view the flow of time.

00:05:08.050 --> 00:05:13.200
However, on a micro scale, if we look at deep down into the physics that make up the universe,

00:05:13.920 --> 00:05:18.080
entropy and - subsequently - time isn't so obvious.

00:05:18.300 --> 00:05:22.280
If I were to record myself cracking an egg and pouring its guts into a bowl

00:05:22.280 --> 00:05:27.520
and then I reversed the footage, you would easily be able to tell that the footage had been reversed.

00:05:27.760 --> 00:05:34.260
However, if I record a pendulum swinging back and forth for five minutes and then reversed the footage and show it to a random person on the street,

00:05:35.350 --> 00:05:38.070
will they be able to tell that that footage has been reversed?

00:05:38.710 --> 00:05:40.799
The answer is probably not.

00:05:41.410 --> 00:05:45.029
See the arrow of time seems to flow in one direction on the macro scale,

00:05:45.490 --> 00:05:50.699
but as you take parts of it away and skim it down to the bare bones of particles that make up the universe,

00:05:51.370 --> 00:05:54.090
time seems to work and flow in every direction;

00:05:54.730 --> 00:05:56.730
both forward and backward.

00:05:57.430 --> 00:06:01.380
There are no laws of physics that states the past is any different from the future.

00:06:02.560 --> 00:06:07.589
The only reason why you can think about what you want to have for dinner tomorrow as opposed to what you want to have for dinner yesterday

00:06:07.620 --> 00:06:09.620
is because of the arrow of time;

00:06:10.180 --> 00:06:13.860
because of entropy, because the universe had a beginning.

00:06:14.380 --> 00:06:16.380
Or at least it seems like it.

00:06:16.450 --> 00:06:20.789
You might be starting to see why the arrow of time and entropy are so important.

00:06:21.220 --> 00:06:24.749
They quite literally govern our lives and the universe.

00:06:25.690 --> 00:06:31.230
See the fact that entropy is increasing is well known. It's the reason why life today is the way that it is.

00:06:32.140 --> 00:06:38.460
However, not many people are addressing the question that is: "Why was the entropy of the universe so low in the first place?"

00:06:39.130 --> 00:06:42.960
Well, the answer is simple. It was lower yesterday than it was today.

00:06:43.720 --> 00:06:46.520
You can take this logic all the way back to the Big Bang.

00:06:47.340 --> 00:06:51.240
You hear that a lot, "The universe came into being at the instance of the Big Bang."

00:06:51.740 --> 00:06:54.560
And for all we know as of now that may be true.

00:06:54.940 --> 00:06:57.340
However, it might not be true.

00:06:57.820 --> 00:07:03.420
We have the physics of Einstein's general relativity that allow us to go back to mere seconds after the Big Bang.

00:07:03.880 --> 00:07:07.120
But after that, our equations break down.

00:07:07.460 --> 00:07:09.300
That's as far as we can go...

00:07:09.620 --> 00:07:10.460
for now.

00:07:10.980 --> 00:07:15.120
There's no law of physics yet that states that there wasn't time before the Big Bang,

00:07:15.180 --> 00:07:17.580
and perhaps a reversed arrow of time.

00:07:17.820 --> 00:07:21.040
We just don't have the science to look that far back yet.

00:07:21.340 --> 00:07:24.420
In a previous video of mine entitled, "The End of Our Universe",

00:07:24.420 --> 00:07:26.900
I discussed the situation that may occur in the

00:07:26.900 --> 00:07:27.660
distant,

00:07:27.660 --> 00:07:28.820
distant future.

00:07:29.280 --> 00:07:33.080
Because the universe is expanding and because entropy is increasing with time,

00:07:33.550 --> 00:07:40.859
there will eventually be a time where everything in the universe is so far apart from one another that space will essentially be empty.

00:07:41.160 --> 00:07:47.540
Everything will be too far apart to interact with one another all the way down to the atoms that make up everything in the universe.

00:07:48.540 --> 00:07:54.680
However, just as the temperature outside fluctuates day to day, so does the entropy of the universe.

00:07:55.480 --> 00:08:00.140
Albeit, very small fluctuations are small time scales such as a human life,

00:08:00.140 --> 00:08:05.100
over unreal time scale such as 10 to the 10 to the 10 to the 56 years.

00:08:05.300 --> 00:08:10.640
It is possible that quantum fluctuations could cause an extremely random extreme entropy decrease.

00:08:11.200 --> 00:08:14.789
This would create conditions similar to the Big Bang as we know it,

00:08:14.980 --> 00:08:18.340
and could explain the arrow of time and the origin of our universe.

00:08:18.720 --> 00:08:24.280
However, in order to answer these questions, we need to unite quantum mechanics with Einstein's general relativity.

00:08:24.640 --> 00:08:31.320
This would provide a scientific link between the quantum world of atoms with the macro world of stars, galaxies and black holes in the universe.

00:08:32.080 --> 00:08:37.160
This is dubbed "the theory of everything" and is something that many scientists are working on right now.

00:08:37.860 --> 00:08:41.660
With this theory, we may be able to - for the first time -

00:08:42.100 --> 00:08:46.520
be able to explain how and why the universe we live in came into existence.

00:08:47.200 --> 00:08:48.339
And maybe,

00:08:48.339 --> 00:08:50.519
even prove that the multiverse exists.

00:08:53.030 --> 00:08:57.939
If you'd like to help in finding a theory of everything and go down in history as a scientific genius,

00:08:57.940 --> 00:09:01.630
then you'll definitely need to study up on relativity as well as the quantum world.

00:09:02.000 --> 00:09:04.869
You can do both of these things over a brilliant.org

00:09:05.390 --> 00:09:11.830
Brilliant has tons of online courses on subjects such as special relativity in quantum objects all the way to calculus and group theory

00:09:12.200 --> 00:09:14.950
These courses are actually fun and interesting to go through,

00:09:15.500 --> 00:09:17.500
unlike your boring math class in school.

00:09:17.600 --> 00:09:21.129
Brilliant show you exactly what you need to know and why you need to know it

00:09:21.280 --> 00:09:24.720
without interjecting a bunch of boring and useless information in between.

00:09:25.340 --> 00:09:31.720
You can learn about some of the most challenging topics and ideas and end up with a really good understanding of any subject in as little as 30 minutes.

00:09:31.780 --> 00:09:36.180
If you'd like to learn more about the world of math and science and support the channel at the same time

00:09:36.180 --> 00:09:37.920
head over to brilliant.org/Aperture

00:09:38.120 --> 00:09:40.560
and start learning about topics that I covered in this video today!

00:09:41.180 --> 00:09:45.489
The first 200 people to go to that link for an 20% off of a premium subscription.

00:09:48.140 --> 00:09:54.580
This video was made in collaboration with the YouTube channel Questn, all the animations you saw in this video were created by them

00:09:54.580 --> 00:09:56.510
and I'm extremely happy to have worked with them

00:09:56.620 --> 00:10:01.660
They actually make great science content,  much like I do,  and are relatively unnoticed

00:10:01.840 --> 00:10:03.340
Their link is in the description below.

00:10:03.500 --> 00:10:08.080
Be sure to leave a comment letting them know I sent you; I'll be making an appearance on their channel very soon.

